import RPi.GPIO as GPIO
import time
import os

# Configure the PIN # 8
GPIO.setmode(GPIO.BOARD)
GPIO.setup(8, GPIO.OUT)
GPIO.setup(12, GPIO.IN, pull_up_down = GPIO.PUD_UP) 
GPIO.setwarnings(False)


# define Shutdown function (restart commented-out)
def Shutdown(channel):  
   #os.system("sudo shutdown -h now")
   print ('press')
#def Restart(channel):
#   os.system("sudo shutdown -r now")

#listen to pressed_button
#GPIO.add_event_detect(12, GPIO.FALLING, callback = Shutdown, bouncetime = 2000)


# Blink Interval 
blink_interval = .5 #Time interval in Seconds

# Blinker Loop
while True:
 GPIO.output(8, True)
 time.sleep(blink_interval)
 GPIO.output(8, False)
 time.sleep(blink_interval)
 print ('Test')
 GPIO.add_event_detect(12, GPIO.FALLING, callback = Shutdown, bouncetime = 2000)


# Release Resources
GPIO.cleanup()